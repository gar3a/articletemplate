import React, {useEffect} from 'react';
import { StyleSheet, View, Image } from 'react-native';

SplashScreen = props => {
    useEffect(() => {
        _bootstrapAsync()
    }, [])  

 const _bootstrapAsync = async () => {
     setTimeout(() => {
         props.navigation.navigate('App');
     }, 2000);
    
  }

    return (
        <View style={style.container}>
            <Image source={require('../assets/images/loader.gif')} style={style.image} />
        </View>
    );
}

const style = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: '#E6F1F1'
    },
    image: {
        width: 150,
        height: 150
    }
});

export default SplashScreen;
