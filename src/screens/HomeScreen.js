import React from 'react'
import { Button, Card } from 'react-native-paper';
import { View } from 'react-native';
import { ScrollView, TouchableOpacity } from 'react-native';
import data from '../api/stories';





export default function HomeScreen(props) {

    return (
        <View>
            <ScrollView>
                {
                    data.map(story => {
                        return (
                            <TouchableOpacity
                                onPress={() => props.navigation.navigate('SinglePost', { story: story })} key={story.id}
                            >
                                <Card style={{ margin: 10, elevation: 5 }}  >
                                    <Card.Title title={story.title} />
                                    <Card.Cover source={story.image} />
                                </Card>
                            </TouchableOpacity>
                        )
                    })
                }
            </ScrollView>



        </View>
    )
}
