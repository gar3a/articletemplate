import React from 'react'
import { View, TouchableOpacity, Text } from 'react-native'
import HeaderImageScrollView, { TriggeringView } from 'react-native-image-header-scroll-view';
const MIN_HEIGHT = 22;
import { AdMobInterstitial } from 'react-native-admob'

export default function PostScreen(props) {

  React.useEffect(() => {
    AdMobInterstitial.setAdUnitID('ca-app-pub-4858214291604947/1889146990');
    AdMobInterstitial.setTestDevices([AdMobInterstitial.simulatorId]);
    AdMobInterstitial.requestAd().then(() => AdMobInterstitial.showAd());
}, [])

  const { navigation } = props;
  const story = navigation.getParam('story');

  return (
    <HeaderImageScrollView maxHeight={200} minHeight={MIN_HEIGHT} headerImage={story.image}
      renderForeground={() => (
        <View style={{ height: 150, justifyContent: "center", alignItems: "center" }} >
          <TouchableOpacity onPress={() => console.log("tap!!")}>
            <Text style={{ backgroundColor: "transparent", fontSize: 25, fontWeight: "bold", textShadowColor: 'rgba(255, 255, 255, 0.9)',
                           textShadowOffset: {width: 1, height: 1}, textShadowRadius: 10 }}>
              {story.title}
            </Text>
          </TouchableOpacity>
        </View>
      )}
    >
      <View style={{ height: 800 }}>
        <TriggeringView onHide={() => console.log("text hidden")}>
          <Text style={{padding:10, textAlign: "center", fontSize: 20}}>
          {story.text}
          </Text>
        </TriggeringView>
      </View>
    </HeaderImageScrollView>
  )
}
