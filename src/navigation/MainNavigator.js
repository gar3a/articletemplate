import { createAppContainer, createSwitchNavigator } from 'react-navigation';
import SplashScreen from '../screens/SplashScreen';
import AppStack from './AppStack';


export default createAppContainer(
  createSwitchNavigator(
    {
      SplashScreen: SplashScreen,
      App: AppStack
    },
    {
      initialRouteName: 'SplashScreen',
    }
  )
);