import React from 'react';
import { createStackNavigator } from 'react-navigation-stack';
import HomeScreen from '../screens/HomeScreen';
import PostScreen from '../screens/PostScreen';
import { AdMobBanner } from 'react-native-admob'

const AppStack = createStackNavigator({ 
    Home: {
      screen : HomeScreen,
      navigationOptions: {
        headerTitle: () => (<AdMobBanner 
                              adSize="smartBanner" adUnitID="ca-app-pub-4858214291604947/5460177605"/>
                            )
    }
  },
    SinglePost: {
      screen : PostScreen
    }  
  }
);

export default AppStack;