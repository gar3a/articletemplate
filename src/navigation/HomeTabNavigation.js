import React from 'react'
import { createAppContainer } from 'react-navigation'
import { createBottomTabNavigator } from 'react-navigation-tabs'
import Icon from 'react-native-vector-icons/FontAwesome'
import HomeScreen from '../screens/HomeScreen'



const HomeTabNavigation = createBottomTabNavigator(
    {
        Home: {
            screen: HomeScreen,
            navigationOptions: {
                headerTitle: ({ focused, tintColor }) => (
                    focused ? <Icon name="globe" color={tintColor} size={28} /> : <Icon name="globe" color={tintColor} size={24} />
                )
            }
        }
    },
    {
        initialRouteName: "Home",
        tabBarOptions: {
            showLabel: false,
            activeTintColor: '#FE7058',
            inactiveTintColor: '#444444'
        }
    }
)

export default createAppContainer(HomeTabNavigation)