
import React from 'react';
import MainNavigator from './navigation/MainNavigator';
import { DefaultTheme, Provider as PaperProvider } from 'react-native-paper';

const theme = {
  ...DefaultTheme,
  roundness: 2,
  colors: {
    ...DefaultTheme.colors,
    primary: '#3498db',
    accent: '#f1c40f',
  },
};

const App: () => React$Node = () => {
  return (
    <PaperProvider theme={theme}>
      <MainNavigator/>
    </PaperProvider>
  );
};


export default App;
